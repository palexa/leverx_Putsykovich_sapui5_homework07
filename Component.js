sap.ui.define([
    "sap/ui/core/UIComponent",
    "sap/ui/model/odata/v2/ODataModel"

], function (UIComponent, ODataModel) {
    "use strict";
    return UIComponent.extend("sap.ui.putsykovich.alexei.Component", {
        metadata : {
            manifest : "json"
        },
        init : function () {
            // call the init function of the parent
            UIComponent.prototype.init.apply(this, arguments);
            // set data model
            var oODataModel = new ODataModel("http://localhost:3000/odata/", {
                useBatch: false,
                defaultBindingMode: "TwoWay"
            });
var mHeaders=oODataModel.getHeaders();
mHeaders["Access-Control-Allow-Origin"]="*";
oODataModel.setHeaders(mHeaders);
oODataModel.read('/Orders');
this.setModel(oODataModel,"odata");

            // create the views based on the url/hash
            this.getRouter().initialize();
        }
    });
});