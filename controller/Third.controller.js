sap.ui.define([
        "sap/ui/core/mvc/Controller",
        "sap/ui/core/routing/History",
        "sap/m/MessageToast",
        "sap/m/MessageBox",
        "sap/ui/model/json/JSONModel"
    ], function (Controller, History,MessageToast, MessageBox,JSONModel) {
        "use strict";

        return Controller.extend("sap.ui.putsykovich.alexei.controller.Third", {

            onInit: function () {

                var oComponent = this.getOwnerComponent();

                var oRouter = oComponent.getRouter();

                oRouter.getRoute("thirdPage").attachPatternMatched(this.onPatternMatched, this);

            },
            onPatternMatched: function (oEvent) {
                var that = this;

                var mRouteArguments = oEvent.getParameter("arguments");

                var sProductID = mRouteArguments.productId;

                var oODataModel = this.getView().getModel("odata");

                oODataModel.metadataLoaded().then(function () {

                    var sKey = oODataModel.createKey("/OrderProducts", {id: sProductID});

                    that.getView().bindObject({
                        path: sKey,
                        model: "odata"
                    });
                });

            },
            onCreateCommentPress: function (oEvent) {
                var oODataModel = this.getView().getModel("odata");
                var oCtx        = oEvent.getSource().getBindingContext("odata");
                var sProductID  = oCtx.getObject().id;
                // unfortunately the sample odata service does not create an ID automatically, so it has to be done
                // manually
                var ID = function () {
                    // Math.random should be unique because of its seeding algorithm.
                    // Convert it to base 36 (numbers + letters), and grab the first 9 characters
                    // after the decimal.
                    return '_' + Math.random().toString(36).substr(2, 9);
                };
                var timeNow=new Date().toISOString();
                var mComment=  {
                    "comment": this.byId('commentText').getValue(),
                    "author": this.byId('commentAuthor').getValue(),
                    "createdDate": timeNow,
                    "rating": this.byId('commentRating').getValue(),
                    "id":ID(),
                    "productId": sProductID
                };


                console.log(mComment);
                // execute "create" request
                oODataModel.create("/ProductComments", mComment, {
                    success: function () {
                        MessageToast.show("Comment was successfully created!")
                    },
                    error: function () {
                        MessageBox.error("Error while creating comment!");
                    }
                });
                this.byId('commentAuthor').setValue(' ');
                this.byId('commentRating').setValue(0);
            },
            onNavBack: function () {
                var oHistory = History.getInstance();
                var sPreviousHash = oHistory.getPreviousHash();

                if (sPreviousHash !== undefined) {
                    window.history.go(-1);
                } else {
                    var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                    oRouter.navTo("secondPage", {}, true);
                }
            }
        });
    }
);
