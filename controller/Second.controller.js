sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/routing/History",
    "sap/m/MessageToast",
    "sap/m/MessageBox"
], function (Controller, History,ODataModel,MessageToast, MessageBox) {
    "use strict";
    return Controller.extend("sap.ui.putsykovich.alexei.controller.Second", {
        onInit:function () {
            var oComponent=this.getOwnerComponent();
            var oRouter=oComponent.getRouter();
            oRouter.getRoute("secondPage").attachPatternMatched(this.onPatternMatched,this);

        },

        onPatternMatched:function (oEvent) {
          var that=this;
          var mRouteArguments=oEvent.getParameter("arguments");
          var sOrderID=mRouteArguments.orderId;
          var oODataModel=this.getView().getModel("odata");
          oODataModel.metadataLoaded().then(function () {
              var sKey=oODataModel.createKey("/Orders",{id:sOrderID});
              that.getView().bindObject({
                  path:sKey,
                  model:"odata"
              });
          });
        },

        onProductPress: function (oEvent) {
            // get the source control of event object (the one that was fired event)
            var oSource = oEvent.getSource();

            // get the binding context of a button (it's a part of the table line, so it inherits the context of it)
            var oCtx = oSource.getBindingContext("odata");


            // get the component
            var oComponent = this.getOwnerComponent();
            oComponent.getRouter().navTo("thirdPage", {
                productId: oCtx.getObject("id")
            });
        },

        onShipChangeButtonPress:function () {
            var oShipView = this.getView();

            // if the dialog was not created before, then create it (lazy loading)
            if (!this.oShipDialog) {
                // use the xmlfragment factory function to get the controls from fragment
                // 1. it is recommended to pass the parent view's id as a first parameter to establish correct
                // prefixing of the controls' id's inside the fragment
                // 2. as an optional third parameter, the link to the object that will be used as a source of event
                // handlers can be passed
                this.oShipDialog = sap.ui.xmlfragment(oShipView.getId(), "sap.ui.putsykovich.alexei.view.fragments.ShipInfoDialog", this);

                // call the "addDependent" method in order to propagate all models and bindings from the view to
                // the controls from fragment
                oShipView.addDependent(this.oShipDialog);
            }

            // set context to the dialog
            this.oShipDialog.bindObject({
                path: "/formFields"
            });

            // open the dialog
            this.oShipDialog.open();
        },

        onCustomerChangeButtonPress:function () {
            var oCustomerView = this.getView();

            // if the dialog was not created before, then create it (lazy loading)
            if (!this.oCustomerDialog) {
                // use the xmlfragment factory function to get the controls from fragment
                // 1. it is recommended to pass the parent view's id as a first parameter to establish correct
                // prefixing of the controls' id's inside the fragment
                // 2. as an optional third parameter, the link to the object that will be used as a source of event
                // handlers can be passed
                this.oCustomerDialog = sap.ui.xmlfragment(oCustomerView.getId(), "sap.ui.putsykovich.alexei.view.fragments.CustomerInfoDialog", this);

                // call the "addDependent" method in order to propagate all models and bindings from the view to
                // the controls from fragment
                oCustomerView.addDependent(this.oCustomerDialog);
            }

            // set context to the dialog
            this.oCustomerDialog.bindObject({
                path: "/formFields"
            });

            // open the dialog
            this.oCustomerDialog.open();
        },

        onAddButtonPress:function () {
            var oView = this.getView();

            // if the dialog was not created before, then create it (lazy loading)
            if (!this.oProductDialog) {
                // use the xmlfragment factory function to get the controls from fragment
                // 1. it is recommended to pass the parent view's id as a first parameter to establish correct
                // prefixing of the controls' id's inside the fragment
                // 2. as an optional third parameter, the link to the object that will be used as a source of event
                // handlers can be passed
                this.oProductDialog = sap.ui.xmlfragment(oView.getId(), "sap.ui.putsykovich.alexei.view.fragments.ProductDialog", this);

                // call the "addDependent" method in order to propagate all models and bindings from the view to
                // the controls from fragment
                oView.addDependent(this.oProductDialog);
            }

            // set context to the dialog
            this.oProductDialog.bindObject({
                path: "/formFields"
            });

            // open the dialog
            this.oProductDialog.open();
        },

        onShipCancelPress: function () {

            this.oShipDialog.close();
            },

        onCustomerCancelPress: function () {
            this.oCustomerDialog.close();
        },

        onAddCancelPress: function () {
            this.oProductDialog.close();
        },

        onCreateProductPress: function (oEvent) {
            var oODataModel = this.getView().getModel("odata");
            var oCtx        = oEvent.getSource().getBindingContext("odata");
            var sOrderID  = oCtx.getObject().id;
            //To create uniq ID
            var ID = function () {
                // Math.random should be unique because of its seeding algorithm.
                // Convert it to base 36 (numbers + letters), and grab the first 9 characters
                // after the decimal.
                return '_' + Math.random().toString(36).substr(2, 9);
            };
            // unfortunately the sample odata service does not create an ID automatically, so it has to be done
            // manually
            var mProduct = {
                "name":  this.byId('name').getValue(),
                "price":  this.byId('price').getValue(),
                "quantity": this.byId('quantity').getValue(),
                "totalPrice":  this.byId('totalPrice').getValue(),
                "id":ID(),
                "orderId":sOrderID
            };

            // execute "create" request
            oODataModel.create("/OrderProducts", mProduct, {
                success: function () {
                    MessageToast.show("Product was successfully created!")
                },
                error: function () {
                    MessageBox.error("Error while creating product!");
                }
            });
            this.oProductDialog.close();
        },

        onUpdateSupplierPress: function (oEvent) {
            var oCtx = oEvent.getSource().getBindingContext("odata");
            var oODataModel = oCtx.getModel();
            var sKey = oODataModel.createKey("/Orders", oCtx.getObject());

            var oCustomer= {"customerInfo": {
                    "firstName": this.byId('custName').getValue().toString(),
                    "lastName": this.byId('custLName').getValue().toString(),
                    "address": this.byId('custAddress').getValue().toString(),
                    "phone": this.byId('custPhone').getValue().toString(),
                    "email": this.byId('custEmail').getValue().toString()
                }};

            oODataModel.update(sKey, oCustomer, {
                success: function () {
                    MessageToast.show("Customer info was successfully updated!")
                },
                error: function () {
                    MessageBox.error("Error while updating customer info!");
                }
            });
            this.onCustomerCancelPress();
        },

        onEditShipPress: function (oEvent) {
            var oCtx = oEvent.getSource().getBindingContext("odata");
            var oODataModel = oCtx.getModel();
            var sKey = oODataModel.createKey("/Orders", oCtx.getObject());

            var oShipTo = {"shipTo": {
                    "name": this.byId('shipName').getValue().toString(),
                    "address": this.byId('shipAddress').getValue().toString(),
                    "ZIP": this.byId('shipZIP').getValue().toString(),
                    "region": this.byId('shipRegion').getValue().toString(),
                    "country": this.byId('shipCountry').getValue().toString()
                }};

            oODataModel.update(sKey, oShipTo, {
                success: function () {
                    MessageToast.show("Ship info was successfully updated!")
                },
                error: function () {
                    MessageBox.error("Error while updating ship info!");
                }
            });
            this.onShipCancelPress();
        },

        onDeleteProductPress: function (oEvent) {
            var oCtx = oEvent.getParameter("listItem").getBindingContext("odata");
            var oODataModel = oCtx.getModel();
            var sKey = oODataModel.createKey("/OrderProducts", oCtx.getObject());

            // execute "delete" request of the entity, specified in a key
            oODataModel.remove(sKey, {
                success: function () {
                    MessageToast.show("Product was successfully removed!")
                },
                error: function () {
                    MessageBox.error("Error while removing product!");
                }
            });
        },

        onNavBack: function () {
            var oHistory = History.getInstance();
            var sPreviousHash = oHistory.getPreviousHash();

            if (sPreviousHash !== undefined) {
                window.history.go(-1);
            } else {
                var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                oRouter.navTo("overview", {}, true);
            }
        }
    });
});