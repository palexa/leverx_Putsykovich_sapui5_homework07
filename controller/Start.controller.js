sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
    "sap/m/MessageToast",
    "sap/m/MessageBox",
    "sap/ui/model/Sorter",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator"
], function (Controller, JSONModel, MessageToast, MessageBox, Sorter, Filter, FilterOperator) {
    "use strict";

    return Controller.extend("sap.ui.putsykovich.alexei.controller.Start", {
        onInit : function () {
            var oOrderModel = new JSONModel({
                summary: {
                    createdAt: "2018-01-01",
                    customer: "Customer",
                    status: "pending",
                    shippedAt: "2018-01-01",
                    totalPrice: 0,
                    currency: "EUR"
                },
                shipTo: {
                    name: "Name",
                    address: "Address",
                    ZIP: "123453",
                    region: "Regin",
                    country: "Country"
                },
                customerInfo: {
                    firstName: "Name",
                    lastName: "Surname",
                    address: "Address",
                    phone: "333",
                    email: "example@gmail.com"
                }
            });
            this.getView().setModel(oOrderModel, "orderModel");
            var oViewModel = new JSONModel({pending: 0, accepted: 0, countAll: 0});
            this.getView().setModel(oViewModel, "orderListView");
            this._oTable = this.byId("orderTable");
            window.abc=this._oTable;
            this._mFilters = {
                "pending": [new  Filter("summary/status", FilterOperator.EQ,  "\'pending\'")],
                "accepted": [new Filter("summary/status", FilterOperator.EQ,   "\'accepted\'")],
                "all": []
            };
        },

        onQuickFilter: function(oEvent ) {
            var oBinding = this._oTable.getBinding("items"),
                sKey = oEvent.getParameter("selectedKey");

            oBinding.filter(this._mFilters[sKey]);
        },

        onUpdateFinished: function() {
            var oViewModel = this.getView().getModel("orderListView");
            var oODataModel = this.getView().getModel("odata");

            oODataModel.read("/Orders/$count", {
                success: function (oData) {
                    oViewModel.setProperty("/countAll", oData);
                }
            });
            oODataModel.read("/Orders/$count", {
                success: function (oData) {
                    oViewModel.setProperty("/pending", oData);
                },
                filters: this._mFilters.pending
            });
            oODataModel.read("/Orders/$count", {
                success: function(oData){
                    oViewModel.setProperty("/accepted", oData);
                },
                filters: this._mFilters.accepted
            });
        },

        onOpenOrderDialogPress:function () {
            var oOrderView = this.getView();

            // if the dialog was not created before, then create it (lazy loading)
            if (!this.oOrderDialog) {
                // use the xmlfragment factory function to get the controls from fragment
                // 1. it is recommended to pass the parent view's id as a first parameter to establish correct
                // prefixing of the controls' id's inside the fragment
                // 2. as an optional third parameter, the link to the object that will be used as a source of event
                // handlers can be passed
                this.oOrderDialog = sap.ui.xmlfragment(oOrderView.getId(), "sap.ui.putsykovich.alexei.view.fragments.OrderDialog", this);

                // call the "addDependent" method in order to propagate all models and bindings from the view to
                // the controls from fragment
                oOrderView.addDependent(this.oOrderDialog);
            }

            // set context to the dialog
            this.oOrderDialog.bindObject({
                path: "/formFields"
            });

            // open the dialog
            this.oOrderDialog.open();
        },

        onCancelPress:function () {
            this.oOrderDialog.close();
        },

        onCreateOrderPress:function () {

            var oODataModel = this.getView().getModel("odata");
            var oOrderModel= this.getView().getModel("orderModel");

            var mOrder=  {
                "summary": {
                    "createdAt": oOrderModel.getProperty("/summary/createdAt").split(',')[0],
                    "customer": oOrderModel.getProperty("/summary/customer"),
                    "status": oOrderModel.getProperty("/summary/status"),
                    "shippedAt": oOrderModel.getProperty("/summary/shippedAt").split(',')[0],
                    "totalPrice": 0,
                    "currency": oOrderModel.getProperty("/summary/currency")
                },
                "shipTo": {
                    "name": oOrderModel.getProperty("/shipTo/name"),
                    "address": oOrderModel.getProperty("/shipTo/address"),
                    "ZIP": oOrderModel.getProperty("/shipTo/ZIP"),
                    "region": oOrderModel.getProperty("/shipTo/region"),
                    "country": oOrderModel.getProperty("/shipTo/country")
                },
                "customerInfo": {
                    "firstName": oOrderModel.getProperty("/customerInfo/firstName"),
                    "lastName": oOrderModel.getProperty("/customerInfo/lastName"),
                    "address": oOrderModel.getProperty("/customerInfo/address"),
                    "phone": oOrderModel.getProperty("/customerInfo/phone"),
                    "email": oOrderModel.getProperty("/customerInfo/email")
                }
            };

            oODataModel.create("/Orders", mOrder, {
                success: function () {
                    MessageToast.show("Order was successfully created!")
                },
                error: function () {
                    MessageBox.error("Error while creating order!");
                }
            });

            this.onCancelPress();
        },

        onOrderPress: function (oEvent) {
            // get the source control of event object (the one that was fired event)
            var oSource = oEvent.getSource();

            // get the binding context of a button (it's a part of the table line, so it inherits the context of it)
            var oCtx = oSource.getBindingContext("odata");


            // get the component
            var oComponent = this.getOwnerComponent();
            oComponent.getRouter().navTo("secondPage", {
                orderId: oCtx.getObject("id")
            });
        },

        onDeleteOrder: function (oEvent) {

            var oCtx = oEvent.getParameter("listItem").getBindingContext("odata");
            var oODataModel = oCtx.getModel();
            var sKey = oODataModel.createKey("/Orders", oCtx.getObject());

            oODataModel.remove(sKey, {
                success: function () {
                    MessageToast.show("Orders was successfully removed!")
                },
                error: function () {
                    MessageBox.error("Error while removing order!");
                }
            });
        }
    });
});

